package br.com.vraptormavenjpa.controller;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.vaptormavenjpa.interfaces.Public;
import br.com.vraptormavenjpa.dao.UserSession;
import br.com.dao.UsuarioDao;
import br.com.vraptormavenjpa.modelo.Usuario;

@Resource
public class LoginController
{
    private Result result;
    private UserSession userSession;
    private UsuarioDao business ;

    public LoginController(Result result, UserSession userSession, UsuarioDao business) {
        this.result = result;
        this.userSession = userSession;
        this.business = business;
    }
    @Public
    @Get("/telaLogin")
    public void telaLogin(){}
    
    @Public
    @Post("/autenticar")
    public void autenticar(Usuario usuario) {
        Usuario user = business.autenticar(usuario.getLogin(), usuario.getSenha());

        if (user != null) {
            userSession.setUser(user);

            result.redirectTo(IndexController.class).index();
        } else {
            result.include("error", "E-mail ou senha incorreta!").redirectTo(this).telaLogin();
        }
    }
    @Get("/logout")
    public void logout() {
        userSession.logout();
        result.redirectTo(this).telaLogin();
    }
}
