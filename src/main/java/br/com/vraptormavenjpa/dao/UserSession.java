
package br.com.vraptormavenjpa.dao;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import br.com.vraptormavenjpa.modelo.Usuario;

@Component
@SessionScoped
public class UserSession 
{
    private Usuario user;
    
    public boolean isLogged()
    {
        return user != null;
    }
    public void logout()
    {
        user = null;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }
    
}
