package br.com.vraptormavenjpa.dao;

import br.com.caelum.vraptor.Resource;
import br.com.vraptormavenjpa.modelo.Usuario;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

@Resource
public class UsuarioDao {
    
    private final Session session;
    private EntityManager manager;

    public UsuarioDao(Session session,EntityManager manager ) {
        this.session = session;
        this.manager = manager;
    }
    public boolean existeUsuario(Usuario usuario) {
        Usuario encontrado = (Usuario) session.createCriteria(Usuario.class)
                .add(Restrictions.eq("login", usuario.getLogin()))
                .uniqueResult();
        return encontrado != null;
    }
    public void inserir(Usuario usuario) {
        Transaction tx = this.session.beginTransaction();
        this.session.save(usuario);
        tx.commit();
    }
    public Usuario autenticar(String email, String senha) {
        try {
            Query query = manager.createQuery("from Usuario where email = :email and senha = :senha");
            query.setParameter("email", email);
            query.setParameter("senha", senha);
            return (Usuario) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}